#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Rafal Sarniak

import os, sys
import argparse
import in_place

def do_filelist(path, pattern=""):
    lista_plikow = os.listdir(path)
    if len(pattern)>0:
        lista_plikow = [x for x in lista_plikow if (pattern in x)]
    return lista_plikow

def linemod(filepath, do_zmiany, podstawienie, verbose=False):
    with in_place.InPlace(filepath) as file:
        for line in file:
            if do_zmiany in line:
                line = line.replace(do_zmiany, podstawienie)
                if verbose: print("Podmieniono")
            file.write(line)

def worker(path, pattern="", do_zmiany="", podstawienie="", verbose=False):
    lista_plikow = do_filelist(path, pattern)
    if verbose:
        print("Plików spełniających kryteria: %d" % len(lista_plikow) )
    for plik in lista_plikow:
        if verbose: print("Przeszukuję: %s" % plik)
        linemod(path + "/" + plik, do_zmiany, podstawienie, verbose)    

if __name__ == "__main__":
    path         = "./fs"
    pattern      = ".snp"
    do_zmiany    = "rt4,roh 5cm"
    podstawienie = "rt4,roh M"
    verbose      = True
    worker(path, pattern, do_zmiany, podstawienie, verbose)
