#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Rafal Sarniak

import os, sys
from datetime import datetime
import socket
import threading

class Tartar_ObsLog:
    def __init__(self):
        self.soft_name = "FS"
        self.filepath  = "/home/oper/.tartar/histfile.txt"
        self.host = "158.75.6.206"  # alien
        self.port = 60666
        
    def make_log(self, date, band, project, source):
        project = project.strip()
        project = project.replace(" ", "_")
        message_content = "%-26s %-10s %-10s %-15s %s" % (date, self.soft_name, band, project, source)
        self.write_in_log(message_content)
        send_message = threading.Thread(target=self.handle_message,args=(message_content,))
        send_message.start()
    
    def write_in_log(self, content):
        plik = open(self.filepath, 'a')
        plik.write(content + '\n')
        plik.close()
        print("Zapisano log.")
        
    def handle_message(self, message):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((self.host, self.port))
            message = message
            sock.sendall(message.encode())
            print("Wysłano wiadomość log.")
        except:
            print( "Serwer odbierający logi nie działa..." )
        sock.close()
            
    # --- Wyżej jest prawie to samo, co w GAME_ObsLog. Od tąd metody są specyficzne dla tartara.
    
    def sprawdz_argv(self):
        if len(sys.argv)>1:
            self.MIN_ilosc_plikow_DAT = int(sys.argv[1])
        else:
            self.MIN_ilosc_plikow_DAT = 20           # Parametr określający ile plików musi być co najmniej, by je spakować.
            
    def ktory_band(self, project):
        if project in ['metvar', 'mszpoz3', 'ex_oh', 'ex_oh_6030', 'tmsc_md', 'mband_mpg']:
            return "M_band"
        elif project in ['kband', 'kband_mon']:
            return "K_band"
        elif project in ['xband', 'xband_md']:
            return "X_band"
        elif project in ['lband_mpg']:
            return "L_band"
        else:
            return "Unknown"
        
    def main_run(self):
        self.sprawdz_argv()
        
        actual_path = os.getcwd()
        target_path = actual_path.replace("/home/oper/","") 
        target_path = "mylove/" + target_path + "/"
        # Sprawdzam, czy jest cos do roboty. Jesli nie - koncze.
        lista_plikow = os.listdir(actual_path)
        lista_plikow_DAT = [x for x in lista_plikow if x[-3:] == 'DAT']
        if len(lista_plikow_DAT) == 0:
            print("Nothing to do. Bye!")
            sys.exit(0)
        # Jesli jednak jest co robić, jade dalej.
        zbior_celi = set([ x[:-9] for x in lista_plikow_DAT])
        lista_celi = list(zbior_celi)
        
        # Liczenie, czy ilosc plikow nie jest za mała (czy nie trwa obserwacja).
        for source in lista_celi:
            count = 0
            for plik in lista_plikow_DAT:
                if source in plik:
                    count += 1
            if count < self.MIN_ilosc_plikow_DAT:           
                zbior_celi.remove(source)
        
        if len(zbior_celi) == 0:
            print("Nothing to do. Bye!")
            sys.exit(0)
            
        utc = datetime.utcnow() 
        actual_utc_date = "%4d%02d%02d" % (utc.year, utc.month, utc.day)
        # Sprawdzam liste plikow z tego dnia na alien.
        lista_alien = os.popen('ssh alien "ls %s"' % target_path).readlines()
        lista_alien_today = [x.rstrip("\n") for x in lista_alien if actual_utc_date in x]

        for source in zbior_celi:
            # Sprawdzam, czy zrodlo bylo juz obserwowane tego dnia.
            i = 0
            for item in lista_alien_today:
                if source.lower() in item:
                    i += 1
            # Jesli bylo, to sprawdzamy ile i dodajemy numerek do pliku, by nie nadpisywac obserwacji.
            if i > 0:
                komenda = 'tar cvf - %s_* | ssh alien "bzip2 -c > %s%s_%s_%d.tar.bz2"' % (source, target_path, source.lower(), actual_utc_date, i)
            else:
                komenda = 'tar cvf - %s_* | ssh alien "bzip2 -c > %s%s_%s.tar.bz2"' % (source, target_path, source.lower(), actual_utc_date)
            print(komenda)
            os.system(komenda)
            
            band = self.ktory_band( actual_path.replace("/home/oper/","") )
            self.make_log( utc.isoformat(), band, actual_path.replace("/home/oper/",""), source)
        
        print( "-" * 70 )
        print( "Koniec.\nJesli wszystko poszlo dobrze, usun pliki .DAT dla opowiednich zrodel." )
        
        print("Sugestia komendy usuwającej:")
        komenda = "rm "
        for source in zbior_celi:
            komenda += source + "_*.DAT "
        print(komenda)
        print( "-" * 70 )

if __name__ == "__main__":
    ObsLog = Tartar_ObsLog()
    ObsLog.main_run()

