#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Rafal Sarniak

import os, sys
import argparse
import in_place

def unhalt(filepath, time):
    with in_place.InPlace(filepath) as file:
        first = True
        for line in file:
            if "halt" in line:
                if first: 
                    first = False
                    line = line.replace('', '')
                else:
                    line = line.replace('halt', '!+%ds' % time )
            elif "5cm" in line:
                line = line.replace('5cm', 'm')
            file.write(line)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Unhalts mass spectral scripts')
    parser.add_argument('filepath', help='filepath of file to unhalt')
    parser.add_argument('-t', metavar='time', type=int, default=60, help='time in sec to wait insted of halt (default 60)')
    args = parser.parse_args()
    unhalt(args.filepath, args.t)
    

