# Pakiet narzędzi Tartar
Pakiet ten składa się z kilku narzędzi przydatnych przy obserwacjach RT4.

## Tartar

Program Tartar automatycznie buduje komendę kompresującą pliki obserwacyjne *.DAT.
Wywołanie:

tartar

Program sprawdza w jakim katalogu na mylove został uruchomiony i na tej podstawie obiera cel, do którego będzie przesyłać pliki na aliena.
Sprawdza również na alienie, czy dane źródło nie było już tego dnia tarowane - jeśli było, dodaje do nazwy pliku wynikowego odpowiedni numer,
zapobiegając nadpisaniu wcześniejszych obserwacji.

Tartar kompresuje dane obserwacyjne dla wszystkich źródeł z osobna - jeśli operator ma "zaległości" w pakowaniu, program z tym sobie poradzi.

Aktualnie program ma ustawione wewnętrzne ograniczenie na minimalnie 20 plików *.DAT dla pojedynczej obserwacji. 
Oznacza to, że program nie będzie kompresować obserwacji, które trwają i mają jeszcze mniej wyników niż 20. 
Można to zmienić, dodając do wywołania liczbę naturalną, np.:

tartar 30 

spowoduje, że spakują się tylko obserwacje posiadające conajmniej 30 plików. Czyli wywołanie:

tartar 20

jest równoważne z wywołaniem programu bez żadnego parametru.

Do obowiązków operatora należy nadal:
- usuwanie spakowanych plików *.DAT - program ze względów bezpieczeństwa tego nie robi.
- jeśli dana obserwacja została spakowana ponownie (z dodanym numerkiem), operator powinien usunąć dodatkowy plik z aliena.

## Unhalt

Program Unhalt usuwa komendy halt w zbiorczych skryptach fieldsystemu wstawiając zamiast nich opóźnienie, 
jednocześnie zostawiając pierwszą komendę w spokoju.
Domyślnie jest to 60 sekund. Można je zmienić urzywając parametru -t. Przykład:

python unhalt.py -t 30 fs/kain.snp

## Massmod

Program Massmod pozwala na masową modyfikację zawartości wielu plików zawierających wspólny wzór w nazwie.

Przykład wykorzystania: podmiana komendy w skryptach *.snp na właściwą.




